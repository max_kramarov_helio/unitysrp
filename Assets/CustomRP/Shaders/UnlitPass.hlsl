﻿#ifndef CUSTOM_UNLIT_PASS_INCLUDED
#define CUSTOM_UNLIT_PASS_INCLUDED

#include "../ShaderLibrary/Common.hlsl"
#include "UnlitInput.hlsl"

struct Attributes
{
    float3 positionOS : POSITION;
    float2 baseUV : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
    float4 pos : SV_POSITION;
    float2 baseUV : VAR_BASE_UV;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

Varyings UnlitPassVertex(Attributes input)
{
    Varyings output;
    UNITY_SETUP_INSTANCE_ID(input);
    // pos
    float3 positionWS = TransformObjectToWorld(input.positionOS);
    output.pos = TransformWorldToHClip(positionWS);
    // uv
    output.baseUV = TransformBaseUV(input.baseUV);
    // inst id
    UNITY_TRANSFER_INSTANCE_ID(input, output);
    return output;
}

float4 UnlitPassFragment(Varyings input) : SV_TARGET
{
    UNITY_SETUP_INSTANCE_ID(input);
    float4 base = GetBase(input.baseUV);
    #if defined (_CLIPPING)
        clip(base.a - GetCutoff());
    #endif
    return base;
}

#endif