using UnityEngine;
using UnityEngine.Rendering;

namespace CustomRP.Runtime
{
    public class TestRenderPipeline : RenderPipeline
    {
        protected override void Render(ScriptableRenderContext context, Camera[] cameras)
        {
            foreach (var camera in cameras)
            {
                var cmdBuffer = new CommandBuffer()
                {
                    name = "CommandBuffer",
                };

                cmdBuffer.BeginSample("a");
                context.ExecuteCommandBuffer(cmdBuffer);
                cmdBuffer.Clear();

                cmdBuffer.ClearRenderTarget(true, true, Color.clear);

                context.DrawSkybox(camera);

                cmdBuffer.EndSample("a");

                context.ExecuteCommandBuffer(cmdBuffer);

                context.Submit();
            }
        }
    }
}